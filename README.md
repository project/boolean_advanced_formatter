# CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


## INTRODUCTION

Advanced formatter to Boolean field, that provides display only one state.

* For a full description of the module, visit the project page:
  <https://drupal.org/project/boolean_advanced_formatter>

* To submit bug reports and feature suggestions, or to track changes:
  <https://drupal.org/project/issues/boolean_advanced_formatter>


## REQUIREMENTS

Drupal core fields.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > YOUR CONTENT
       TYPE > Manage Display.
    3. Select formatter for your field.
    4. Save configuration.


## MAINTAINERS

* KrakenBite - <https://www.drupal.org/u/KrakenBite>
