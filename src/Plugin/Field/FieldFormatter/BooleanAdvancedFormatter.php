<?php

namespace Drupal\boolean_advanced_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BooleanFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'boolean' formatter.
 *
 * @FieldFormatter(
 *   id = "boolean_advanced_formatter",
 *   label = @Translation("Boolean advanced formatter"),
 *   field_types = {
 *     "boolean",
 *   }
 * )
 */
class BooleanAdvancedFormatter extends BooleanFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['inverse_format_state'] = 0;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['inverse_format_state'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable display for "off" state'),
      '#default_value' => $this->getSetting('inverse_format_state'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $setting = $this->getSetting('inverse_format_state');
    $summary = parent::settingsSummary();
    $summary[] = $setting ? $this->t('Display for "off" state only') : $this->t('Display for "on" state only');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $setting = $this->getSetting('inverse_format_state');

    foreach ($items as $delta => $item) {
      if (!($setting xor $item->value)) {
        unset($elements[$delta]);
      }
    }

    return $elements;
  }

}
